from stack import *
from math import ceil

sym = '*'


def sort(l):
    id = 0
    min = ord(str((l[0])))
    for x in range(len(l)):
        if min > ord(str((l[x]))):
            min = (ord(str((l[x]))))
            id = x
    return id


if __name__ == '__main__':
    print('Lab1 - Semi-static data structures\n'
          'TI-92 Androshcuk Vlad\n')
    temp = Stack()
    for x in range(5):
        temp.push(x)
    print(temp.print())

    # first
    last = temp.get()
    stack1 = temp.print().copy()
    first = stack1[0]
    stack1.pop(0)
    temp.clear()
    temp.push(last)
    for x in stack1:
        temp.push(stack1[x - 1])
    temp.push(first)
    del stack1
    print(temp.print())

    # second
    stack1 = temp.print().copy()
    size = temp.size()
    if size % 2 == 0:
        pl = (round(size / 2))
        stack1.insert(pl, sym)
    else:
        pl = (ceil(size / 2))
        stack1.insert(pl, sym)
    temp.clear()
    for x in stack1:
        temp.push(x)
    del stack1
    print(temp.print())

    # three
    stack1 = temp.print().copy()
    min = sort(stack1)
    stack1.pop(min)
    temp.clear()
    for x in stack1:
        temp.push(x)
    del stack1

    # finally
    print(temp.print())

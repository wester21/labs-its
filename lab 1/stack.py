class Stack:
    def __init__(self):
        self.stack = list()

    def push(self, el):
        self.stack.append(el)

    def print(self):
        return self.stack

    def get(self):
        try:
            t = self.stack[len(self.stack) - 1]
            self.stack.pop(len(self.stack) - 1)
            return t
        except:
            return None

    def clear(self):
        self.stack.clear()

    def size(self):
        return len(self.stack)

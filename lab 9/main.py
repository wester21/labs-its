import random

array = []
author = ["William Shakespeare", "Emily Dickinson", "H. P. Lovecraft", "Arthur Conan Doyle",
          "Leo Tolstoy"]


def genArray(start=0, end=1, rand=False):
    if not rand:
        for x in range(start, end):
            array.append(x)
    if rand:
        for x in range(start, end):
            array.append(random.randint(start, end))


def sortArray(arr, sortDirection=False):
    if not sortDirection:
        for i in range(len(arr) - 1):
            for j in range(len(arr) - i - 1):
                if arr[j] > arr[j + 1]:
                    arr[j], arr[j + 1] = arr[j + 1], arr[j]
    if sortDirection:
        for i in range(len(arr) - 1):
            for j in range(len(arr) - i - 1):
                if arr[j] < arr[j + 1]:
                    arr[j], arr[j + 1] = arr[j + 1], arr[j]
    return arr


def normalizeArray(arr):
    temp = []
    for x in arr:
        temp.append(x.strip().replace(' ', '').replace('.', '').upper())
    return temp


def stringToNumbers(test):
    nums = []
    for x in range(len(test)):
        nums.append(ord(test[x]))
    return nums


def numbersToSting(num):
    nums = ""
    for x in range(len(num)):
        nums = nums+chr(num[x])
    return nums


def sortArrayOfArrays(array, res, position=0):
    lastMin = array[0].copy()
    t = array.copy()
    while True:
        x = (len(t))
        if x == 0:
            break
        min = t[0]
        for index in range(x):
            if t[index][position] < min[position]:
                lastMin = min
                min = t[index]
        res.append(min.copy())
        t.remove(min)
    return res


def filterNumbers(array, filterSwich=False):
    if not filterSwich:
        res = []
        for x in array:
            if x >= 0:
                res.append(x)
        return res
    if filterSwich:
        res = []
        for x in array:
            if x <= 0:
                res.append(x)
        return res


if __name__ == '__main__':
    print("Lab 9")

    # task  1
    print("Task 1: ")
    genArray(-5, 5, True)
    array = filterNumbers(array)
    print(sortArray(array))

    # task 2
    print("Task 2: ")
    author = normalizeArray(author)
    arrays = []
    for x in range(len(author)):
        arrays.append(stringToNumbers(author[x]))
    res = []
    resultAuthor = []
    for x in (sortArrayOfArrays(arrays, res)):
        resultAuthor.append(numbersToSting(x))
    print(resultAuthor)

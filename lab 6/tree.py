class Node:
    def __init__(self, value):
        self._value = value
        self._left = None
        self._right = None

    @property
    def value(self):
        return int(self._value)

    @value.setter
    def value(self, value):
        self._value = int(value)

    @property
    def left(self):
        return self._left

    @left.setter
    def left(self, value):
        self._left = value

    @property
    def right(self):
        return self._right

    @right.setter
    def right(self, value):
        self._right = value

    def __mod__(self, other):
        return int(self.value) % int(other)

    def __str__(self):
        return str(self._value)

    def __int__(self):
        return int(self._value)

    def depth(self, depth=0):
        depth += 1
        left = self.left.depth(depth) if self.left else 0
        right = self.right.depth(depth) if self.right else 0
        if left > depth:
            depth = left
        if right > depth:
            depth = right
        return depth

    def __eq__(self, other):  # ==
        return int(self._value) == int(other)

    def __gt__(self, other):  # >
        return int(self._value) > int(other)

    def __lt__(self, other):  # <
        return int(self._value) < int(other)

    def __len__(self):
        cnt = 1
        if self._left:
            cnt += len(self._left)
        if self._right:
            cnt += len(self._right)

        return cnt

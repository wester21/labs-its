import os

devMode = False
lab = 6


class Labs():
    LAB_1 = 'lab\ 1/'
    LAB_2 = 'lab\ 2/'
    LAB_3 = 'lab\ 3/'
    LAB_4 = 'lab\ 4/'
    LAB_5 = 'lab\ 5/'
    LAB_6 = 'lab\ 6/'
    LAB_7 = 'lab\ 7/'
    LAB_8 = 'lab\ 8/'
    LAB_9 = 'lab\ 9/'
    PATH = './'
    FILE = 'main.py'


labs = [
    Labs.LAB_1,
    Labs.LAB_2,
    Labs.LAB_3,
    Labs.LAB_4,
    Labs.LAB_5,
    Labs.LAB_6,
    Labs.LAB_7,
    Labs.LAB_8,
    Labs.LAB_9,
]

if __name__ == '__main__':
    print('Created by vlad')
    if devMode:
        os.system(f'python {Labs.PATH}{labs[lab-1]}{Labs.FILE}')
    else:
        num = int(input('Select lab number: ')) - 1
        if not (num >= 0 and num < 9):
            print("lab undefined")
            exit(0)
        os.system(f'python {Labs.PATH}{labs[num]}{Labs.FILE}')

import random

array = []


class SearchEnum:
    BINARY = 1
    LINEAR = 2
    INDEXED = 3


def binarySearch(arr, low, high, x):
    if high >= low:
        mid = (high + low) // 2
        if arr[mid] == x:
            return mid
        elif arr[mid] > x:
            return binarySearch(arr, low, mid - 1, x)
        else:
            return binarySearch(arr, mid + 1, high, x)
    else:
        return -1


def search(arr, x):
    for i in range(len(arr)):

        if arr[i] == x:
            return i
    return -1


def indexedSequentialSearch(arr, n, k):
    elements = [0] * 20
    indices = [0] * 20
    j, ind = 0, 0
    for i in range(0, n, 3):
        # Элемент хранения
        elements[ind] = arr[i]
        # Хранение индекса
        indices[ind] = i
        ind += 1
    if k < elements[0]:
        print("Not found")
        exit(0)
    else:
        for i in range(1, ind + 1):
            if k < elements[i]:
                start = indices[i - 1]
                end = indices[i]
                break

    for i in range(start, end + 1):
        if k == arr[i]:
            j = 1
            break

    if j == 1:
        return i
    else:
        return -1


def genArray(start=0, stop=1):
    let = []
    for x in range(start, stop):
        let.append(x)
    return let


def printResult(array, x, func: SearchEnum):
    if func == SearchEnum.BINARY:
        print("Binary search:")
        result = binarySearch(array, 0, len(array) - 1, x)
        if result != -1:
            print("\tIndex:", str(result))
        else:
            print("\tElement is not found")
    if func == SearchEnum.LINEAR:
        print("Linear search:")
        result = search(array, x)
        if result != -1:
            print("\tIndex:", str(result))
        else:
            print("\tElement is not found")
    if func == SearchEnum.INDEXED:
        print("Indexed sequential search:")
        result = indexedSequentialSearch(array, len(array), x)
        if result != -1:
            print("\tIndex:", str(result))
        else:
            print("\tElement is not found")


if __name__ == '__main__':
    print("Lab 4")

    number = [345, 368, 876, 945, 564, 387, 230]
    array = (genArray(stop=10))
    x = 3

    print("Task 1:")
    printResult(array, x, SearchEnum.BINARY)
    printResult(array, x, SearchEnum.LINEAR)
    printResult(array, x, SearchEnum.INDEXED)

    print("Task 2: ")
    printResult(array, x, random.randint(1, 3))
    num = 564
    printResult(number, num, random.randint(1, 3))
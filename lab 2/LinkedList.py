class Box:
    def __init__(self, obj=None):
        self.obj = obj
        self.nextobj = None


class LinkedList:
    def __init__(self):
        self.head = None

    def contains(self, obj):
        lastbox = self.head
        while (lastbox):
            if obj == lastbox.obj:
                return True
            else:
                lastbox = lastbox.nextobj
        return False

    def addToEnd(self, newobj):
        newbox = Box(newobj)
        if self.head is None:
            self.head = newbox
            return
        lastbox = self.head
        while (lastbox.nextobj):
            lastbox = lastbox.nextobj
        lastbox.nextobj = newbox

    def get(self, objIndex):
        lastbox = self.head
        boxIndex = 0
        try:
            while boxIndex <= objIndex:
                if boxIndex == objIndex:
                    return lastbox.obj
                boxIndex = boxIndex + 1
                lastbox = lastbox.nextobj
        except:
            return None
    def removeBox(self, rmobj):
        headobj = self.head

        if headobj is not None:
            if headobj.obj == rmobj:
                self.head = headobj.nextobj
                headobj = None
                return
        while headobj is not None:
            if headobj.obj == rmobj:
                break
            lastobj = headobj
            headobj = headobj.nextobj
        if headobj == None:
            return
        lastobj.nextobj = headobj.nextobj
        headobj = None

    def LLprint(self):
        currentobj = self.head
        print("LINKED LIST\n")
        i = 0
        while currentobj is not None:
            print(str(i) + ": " + str(currentobj.obj))
            i += 1
            currentobj = currentobj.nextobj

from db import DataBase
#           *     *                             *
ticket = [4313, 1254, 1245, 5623, 6234, 2345, 1167]


def getSumm(num):
    result = str(num)
    summ = 0
    for x in range(len(result)-1):
        summ = summ + int(result[x])
    return summ


if __name__ == '__main__':
    print("Lab 8")
    db = DataBase()

    # first task
    print("First task: ")
    dateList = []
    for x in (db.getHumans(order='birthday')):
        dateList.append(x)
        print(x)

    # second task
    print("\nSecond task: ")
    for x in range(1, len(db.getStudentCount()) + 1):
        summ = 0
        for student in db.getAllStudent(filterValue=x, filter='studentid'):
            summ = summ + student[7]
        db.setSummStudent(studentId=student[0], summ=summ)
    print(db.getMarkSumm())

    print("\nThree task: ")
    p = []
    for x in ticket:
        if getSumm(x) == 8:
            p.append(x)
    p = sorted(p)
    print(p)
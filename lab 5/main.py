lst = [1, 2, 3, 4, 5, 2, 1, 8, 9, 10]


def binNum(array):
    max = 0
    for x in range(len(array)):
        if array[x] % 2 == 0 and array[x] > max:
            max = array[x]
    return 'not number' if max == 0 else max


def firstTask(array):
    temp = []
    for x in range(1, len(array) - 1):
        temp.append(
            array[x - 1] / array[x + 1]
        )
    return temp


def secondTask(array):
    temp = []
    for x in range(1, len(array) - 1):
        temp.append(
            array[x - 1] - array[x + 1]
        )
    return temp


if __name__ == '__main__':
    print("first task:", binNum(firstTask(lst)))
    print("second task", binNum(secondTask(lst)))

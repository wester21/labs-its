import random
from tree import Node


def rand():
    return random.randint(-99, 99)


def printTT(tree, prf='', prfChild=''):  # O(n)
    tmp = ''
    tmp += prf + str(tree.value) + '\n'
    if tree.left and tree.right:
        tmp += printTT(tree.left, prfChild + "├── ", prfChild + "│   ")
        tmp += printTT(tree.right, prfChild + "└── ", prfChild + "    ")
    else:
        tmp += printTT(tree.left or tree.right, prfChild + "└── ", prfChild + "    ") if tree.left or tree.right else ''
    return tmp


def fill_tree(el):
    tree = Node(el[0]) if len(el) > 0 else None
    el = el[1:]
    if len(el) >= 2:
        mid = random.randint(0, len(el) - 1)
        # print(str(el[0:mid]) + ' : ' + str(el[mid:]))
        tree.left = fill_tree(el[0:mid])
        tree.right = fill_tree(el[mid:])
    if len(el) == 1:
        tree.left = Node(el[0])
    return tree


def create_filled_sorted(nodes, desc=True):
    els = [random.randint(-99, 99) for x in range(nodes)]
    els.sort()
    if not desc:
        els = els[::-1]
    tree = fill_tree(els)
    return tree


def create_new_ttree(date):
    arr.sort()
    return fill_tree(date)


arr = []


def search(tree):
    if tree % 2:
        tree.value = 100
    if tree.left:
        if tree.value != 100: arr.append(tree.value)
        search(tree.left)
    if tree.right:
        if tree.value != 100: arr.append(tree.value)
        search(tree.right)


if __name__ == '__main__':
    print("Lab 6")

    print("Tree before: ")
    ttree = create_filled_sorted(15, desc=False)
    print(printTT(ttree))

    print("Tree after: ")
    search(ttree)
    print(printTT(ttree))

    print("Tree after delete: ")
    print(arr)
    ttree = create_new_ttree(arr)
    print(printTT(ttree))

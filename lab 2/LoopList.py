class LoopList():
    def __init__(self):
        self.lList = []
        self.element = None
        self.iter = -1
        self.max = 0

    def __check__(self):
        if self.iter + 1 == self.max:
            return 0
        elif self.iter >= self.max:
            return 0
        else:
            return self.iter + 1

    def add(self, el):
        self.lList.append(el)
        self.element = el
        self.iter += 1
        self.max += 1

    def get(self):
        temp = self.element
        self.iter = self.__check__()
        self.element = self.lList[self.iter]
        return temp

    def setToFirst(self):
        self.element = self.lList[0]
        self.iter = 0

    def getLen(self):
        return self.max

    def delete(self):
        self.iter = self.__check__()
        temp = self.lList[self.iter]
        self.lList.pop(self.iter)
        self.max -= 1
        return temp

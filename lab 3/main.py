import math

from tree import Node


def printTT(tree, prf='', prfChild=''):  # O(n)
    tmp = ''
    tmp += prf + str(tree.value) + '\n'
    if tree.left and tree.right:
        tmp += printTT(tree.left, prfChild + "├── ", prfChild + "│   ")
        tmp += printTT(tree.right, prfChild + "└── ", prfChild + "    ")
    else:
        tmp += printTT(tree.left or tree.right, prfChild + "└── ", prfChild + "    ") if tree.left or tree.right else ''
    return tmp


def getLeft(tree):
    if tree.left:
        return getLeft(tree.left)
    return tree.value


def searchX(tree, x):
    if tree is None: return 0
    return (tree.value == x if 1 else 0) + searchX(tree.left, x) + searchX(tree.right, x)


if __name__ == '__main__':
    print("Lab 3")

    ftree = Node('9')
    ftree.right = Node('2')
    ftree.left = Node('1')
    ftree.left.right = Node('4')
    ftree.left.left = Node('3')
    ftree.right.right = Node('6')
    ftree.right.left = Node('3')

    print("Tree: ")
    print(printTT(ftree))

    print("Task 1: ")
    E = getLeft(ftree)
    print("E: ", E)

    print("\nTask 2: ")
    print("count: ", searchX(ftree, E))

from LinkedList import *
from LoopList import *

count = 10
killed = 5


def move(idEl=1, n=1, lList=LinkedList()):
    temp = int()
    newList = LinkedList()
    i = 0
    while lList.get(i) is not None:
        if i == idEl:
            temp = lList.get(i)
            i += 1
            continue
        if i == idEl + n:
            newList.addToEnd(temp)
        newList.addToEnd(lList.get(i))
        i += 1
    newList.LLprint()
    return newList


def gen(count):
    for x in range(1, count):
        lList.addToEnd(x)


def loopListGen(count, lList, obj):
    for x in range(1, count + 1):
        lList.add(obj)


def createNewList(filterKey, filterValue, lList):
    newList = LoopList()
    for i in range(lList.getLen()):
        el = lList.get()
        if el[filterKey] == filterValue:
            newList.add(el)
    return newList


def unit(first, second):
    first.setToFirst()
    for x in range(killed):
        first.delete()
    tempList = LoopList()
    for x in range(first.getLen()):
        tempList.add(first.delete())
    for x in range(second.getLen()):
        tempList.add(second.delete())
    return tempList


def splitSolder(all):
    f = LoopList()
    s = LoopList()

    for x in range(all.getLen()):
        if x % 2:
            f.add(all.get())
        else:
            s.add(all.get())
    return [f, s]


if __name__ == '__main__':
    lList = LinkedList()

    gen(10)
    lList = move(1, 3, lList)

    lList.LLprint()

    loList = LoopList()
    loopListGen(5, loList, {
        'name': 'PlayStation',
        'version': 1,
        'company': 'Sony'
    })
    loopListGen(5, loList, {
        'name': 'XBox',
        'version': 1,
        'company': 'Microsoft'
    })

    for x in range(loList.getLen()):
        print(loList.get())

    loList.setToFirst()

    loList = createNewList('company', 'Sony', loList)
    loList.setToFirst()

    for x in range(5):
        print(loList.get())


    # task 4
    armyListOne = LoopList()
    loopListGen(10, armyListOne, {
        'name': 'Ivan',
        'lastname': 'Ivanov'
    })
    armyListTwo = LoopList()
    loopListGen(10, armyListTwo, {
        'name': 'Ivan',
        'lastname': 'Noivanov'
    })

    allSoldier = unit(armyListOne, armyListTwo)

    temp = splitSolder(allSoldier)
    armyListOne = temp[0]
    armyListTwo = temp[1]

    for x in range(armyListOne.getLen()):
        print(armyListOne.get())
    for x in range(armyListTwo.getLen()):
        print(armyListTwo.get())

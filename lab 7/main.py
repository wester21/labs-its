import random

from db import DataBase
from services import month


def find(arr):
    result = []
    for x in arr:
        counter = 0
        if x in result: continue
        # print(x)
        for i in arr:
            if x == i:
                counter += 1
        if counter >= 3:
            result.append(x)
    return result


if __name__ == '__main__':
    db = DataBase()
    # first task
    print("First task: Вивести за алфавітом у зворотному \nпорядку власників автомобілів \nмарки 'Мерседес'")
    number = []
    for x in db.getManufacturer(sort='desc', order='clientname', filter='manufacturer', filterValue='2'):
        if (x[1] in number):
            None
        else:
            print(x)
        number.append(x[1])

    # second task
    print(
        "\n\nSecond task: Вивести за алфавітом у зворотному\nпорядку імена власників, \nкількість попередніх ремонтів "
        "машин яких більше трьох.")
    names = []
    for x in find(db.getAllNumber()):
        num = "'" + x[0] + "'"
        names.append(db.getClient(number=num.upper(), order='clientname', sort='asc', top=1)[0][3])
    names.sort(reverse=True)
    print(names)

    # sorting first task
    print("\nSorting. First task:")
    for x in range(1, len(db.getStudentCount()) + 1):
        summ = 0
        for student in db.getAllStudent(filterValue=x, filter='studentid'):
            summ = summ + student[7]
        db.setSummStudent(studentId=student[0], summ=summ)

    print(db.getMarkSumm())

    # sorting second task
    print("Sorting. Second task:")
    print(db.getStudentsName(order='studentfirstname', select='studentfirstname'))

    db.close()

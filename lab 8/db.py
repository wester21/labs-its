import psycopg2


class DataBase:
    def __init__(self):
        try:
            # connection settings
            self.connection = psycopg2.connect(
                host='localhost',
                user='vlad',
                password='',
                dbname='base'
            )
            self.cursor = self.connection.cursor()

        except (Exception) as error:
            print("Error while connecting to PostgreSQL", error)

    def getHumans(self, order='id', sort='ASC'):
        self.cursor.execute(
            f"SELECT * FROM humens ORDER BY {order} {sort}")
        return self.cursor.fetchall()

    def newMark(self, student, lesson, mark, session=1):
        self.cursor.execute(
            f"insert into marks (student, lesson, mark, session) values ({student},{lesson}, {mark},{session})")
        self.connection.commit()
        return 'OK'

    def getStudentCount(self):
        self.cursor.execute(f"select * from students")
        return self.cursor.fetchall()

    def getAllStudent(self, filter='', filterValue=''):
        if len(filter) > 1:
            self.cursor.execute(
                f"select * from students join marks m on students.studentId = m.student join lessons l on l.lessonId = m.lesson where {filter}={filterValue}")

        else:
            self.cursor.execute(
                f"select * from students join marks m on students.studentId = m.student join lessons l on l.lessonId = m.lesson")
        return self.cursor.fetchall()

    def getStudentsName(self, order='id', sort='ASC', filter='', filterValue='', select=''):
        if len(filter) > 1:
            self.cursor.execute(
                f"select * from students where {filter}={filterValue} ORDER BY {order} {sort}")
        if len(filter) > 1 and len(select) > 1:
            self.cursor.execute(
                f"select {select} from students where {filter}={filterValue} ORDER BY {order} {sort}")
        elif len(select) > 1:
            self.cursor.execute(
                f"select {select} from students ORDER BY {order} {sort}")
        else:
            self.cursor.execute(
                f"select * from students ORDER BY {order} {sort}")
        return self.cursor.fetchall()

    def setSummStudent(self, studentId, summ, session=1):
        self.cursor.execute(
            f"update marksumm set studentId={studentId}, summ={summ}, session={session} where studentId = {studentId}")
        self.connection.commit()
        return 'OK'

    def getMarkSumm(self):
        self.cursor.execute(
            f"select studentfirstname, studentlastname, summ from marksumm join students s on s.studentId = marksumm.studentId order by summ")
        return self.cursor.fetchall()
